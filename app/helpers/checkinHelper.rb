# frozen_string_literal: true

class CheckinHelper
  def self.register(pnumber)
    v1 = Vehicle.find_by(plateno: pnumber)
    if !v1.nil?
      p = Park.find_by(vehicle_id: 0)
      if p.nil?
        render json: {
          error: 'No Slot available to book'
        }, status: 400
      else
        p.vehicle_id = v1.id
        p.save!
        Ticket.create(checkin: DateTime.now, checkout: DateTime.now, pay: 80, vehicle_id: v1.id, park_id: p.id)
      end
    else
      raise StandardError, 'No Record found'
    end
  end
end
