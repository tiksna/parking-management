# frozen_string_literal: true

class CheckoutHelper
  def self.remove_from_parking(tid)
    t = Ticket.find_by(id: tid)
    if !t.nil?
      t.checkout = DateTime.now
      t.save
      p = Park.find_by(id: t.park_id)
      p.vehicle_id = 0
      p.save!
    else
      render json: {
        error: 'Record Not Found'
      }, status: 404
    end
  end
end
