# frozen_string_literal: true

# notification if checkintime exceeded
class NotifyAfterTwelve
  include Sidekiq::Worker
  sidekiq_option retry: false

  def perform
    # curr_ticket = Ticket.find_by(ticked_id: ticketId)
    checkin_time = Ticket.select([:checkin, :vehicle_id])
    for time in checkin_time
      if((Datetime.now-time.checkin)*24 >= 12)
        puts "The vehicle with id #{time.vehicle_id} needs to checkout"
      end
    end
  end
end
