# frozen_string_literal: true

class Ticket < ApplicationRecord
  belongs_to :vehicle
  belongs_to :park
  validates :checkin, presence: true
  validates :checkout, presence: true
  validates :pay, presence: true
  validates :park_id, presence: true
  validates :vehicle_id, presence: true
end
