# frozen_string_literal: true

class User < ApplicationRecord
  has_many :vehicles, dependent: :destroy
  validates :name, presence:  true
  validates :email, presence: true
  validates :phone, presence: true
end
