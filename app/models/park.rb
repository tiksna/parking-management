# frozen_string_literal: true

class Park < ApplicationRecord
  belongs_to :vehicle

  validates :floor, presence: true
  validates :block, presence: true
  validates :vehicle_id, presence: true
end
