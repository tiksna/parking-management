# frozen_string_literal: true

module API
  module V1
    class Apiusers < Grape::API
      version 'v1'
      format :json
      prefix :api
      resource :users do
        desc 'Return all the users'
        get '', root: :users do
          User.all
        end
        desc 'Create new User'
        params do
          requires :cname, type: String
          requires :cemail, type: String
          requires :cphone, type: Integer
        end
        post do
          User.create!({
                         name: params[:cname],
                         email: params[:cemail],
                         phone: params[:cphone]
                       })
        end
        desc 'Get the Vehicle of the User'
        # localhost:3000/api/v1/users/find?cname=Navneet
        params do
          requires :cname, type: String
        end
        post ':find' do
          user1 = User.find_by(name: params[:cname])
          Vehicle.find_by(user_id: user1.id)
        end

        desc 'Get the Checkin and check out time of the car '
        params do
          requires :cname, type: String
        end
        post ':history' do
          user1 = User.find_by(name: params[cname])
          vehicle1 = Vehicle.find_by(user_id: user1.id)
          park1 = Park.find_by(vehicle_id: vehicle1.id)
          Ticket.find_by(vehicle_id: vehicle1.id, park_id: park1.id)
        end
      end
    end
  end
end
