# frozen_string_literal: true

module API
  module V1
    class Checkinapis < Grape::API
      version 'v1'
      format :json
      prefix :api

      resource :parking do
        desc 'display the tickets'
        get '', root: :parking do
          Ticket.all
        end

        desc 'insert into the check for a vehicle'
        params do
          requires :plateno, type: Integer
        end
        post ':checkin' do
          # find the valid location in the parking that can be alloted
          f1 = Park.where(floor: 2)
          ans = -1
          a = []
          f1.each do |u|
            a.append(u.block)
          end
          (1..50).each do |i|
            next unless (a.include? i) == false

            ans = i
            # print ans
            break
          end
          f1 = Park.where(floor: 1)
          ans1 = -1
          a = []
          f1.each do |u|
            a.append(u.block)
          end
          (1..50).each do |i|
            next unless (a.include? i) == false

            ans1 = i
            # print ans
            break
          end
          # it means there is an empty building here and the car is not already parked in the area
          if (ans1 != -1) || (ans != -1)
            veh_id = Vehicle.find_by(plateno: params[:plateno])
            v1 = Park.find_by(vehicle_id: veh_id.id)
            if (ans != -1) && v1.nil?
              Park.create(floor: 2, block: ans, vehicle: veh_id)
              p = Park.find_by(vehicle_id: veh_id.id)
              Ticket.create(checkin: DateTime.now, checkout: DateTime.now, pay: 10, vehicle_id: veh_id.id,
                            park_id: p.id)

            elsif (ans1 != -1) && v1.nil?
              Park.create(floor: 1, block: ans, vehicle: veh_id)
              p = Park.find_by(vehicle_id: veh_id.id)
              Ticket.create(checkin: DateTime.now, checkout: DateTime.now, pay: 10, vehicle_id: veh_id.id,
                            park_id: p.id)
            end

          end
        end
      end
    end
  end
end
