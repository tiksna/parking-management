# frozen_string_literal: true

module API
  module V1
    # this is class belongs
    class Checkoutapis < Grape::API
      version 'v1'
      format :json
      prefix :api
      # helpers API::V1::Helpers::Checkin_helper
      resource :checkout do
        desc 'display the tickets'
        get '', root: :parking do
          Ticket.all
        end

        desc 'check out the user from the ticket table counter'
        params do
          requires :plateno, type: Integer
        end
        post 'checkout' do
          v1 = Vehicle.find_by(plateno: params[:plateno])
          # v1.id is the vehicle id
          Park.find_by(vehicle_id: v1.id).delete
          # check the multiple entry
          tickets = Ticket.find_by(vehicle_id: v1.id)
          tickets.checkout = DateTime.now
          tickets.save
        end
      end
    end
  end
end
