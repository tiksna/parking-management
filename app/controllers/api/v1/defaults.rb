# frozen_string_literal: true

module API
  module V1
    class Defaults
      def self.initialization
        version 'v1'
        prefix 'api'
        format :json
      end
    end
  end
end
