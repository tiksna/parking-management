# frozen_string_literal: true

module API
  module V1
    class Base < Grape::API
      mount API::V1::Parkings
      mount API::V1::Apiusers
      mount API::V1::Apitickets
      mount API::V1::Checkinapis1
      mount API::V1::Checkoutapis1
    end
  end
end
