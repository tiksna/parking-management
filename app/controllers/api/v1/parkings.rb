# frozen_string_literal: true

module API
  module V1
    class Parkings < Grape::API
      version 'v1'
      format :json
      prefix :api
      # parking details of the user
      resource :parking do
        desc 'Return all the users'
        get '', root: :parking do
          Park.all
        end
        desc 'Getting the parking details of the user'
        params do
          requires :cname, type: String
        end
        get 'parkdetails' do
          user1 = User
          user2 = User.find_by(name: params[:cname])
          veh2 = Vehicle.find_by(user_id: user2.id)
          Park.find_by(vehicle_id: veh2.id)
        end
      end
    end
  end
end
