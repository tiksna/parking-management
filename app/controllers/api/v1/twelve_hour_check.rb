# frozen_string_literal: true

module API
  module V1
    # this is the twelve hour check function
    class CheckTwelveHour < Grape::API
      version 'v1'
      prefix 'api'
      format :json
      resource :checktimeout do
        desc 'check if any of the car is parked for more that twelve hours'
        get 'check' do
          NotifyAfterTwelve.perform_async
          render text: 'The checkintimeout process is added to the queue'
        end
      end
    end
  end
end
