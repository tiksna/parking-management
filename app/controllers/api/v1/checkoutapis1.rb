# frozen_string_literal: true

require_relative '../application/presenters/checkout_api'

module API
  module V1
    # this is class belongs
    class Checkoutapis1 < Grape::API
      version 'v1'
      prefix 'api'
      format :json
      resource :parks do
        desc 'check out the user from the ticket table counter'
        params do
          requires :ticketID, type: Integer
        end
        get 'checkout' do
          checkout = API::Application::Presenters::CheckoutAPI.new(params[:ticketID]).remove
          # CheckoutHelper.remove_from_parking(params[:ticketID])
        end
      end
    end
  end
end
