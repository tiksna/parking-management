# frozen_string_literal: true

module API
  module V1
    class Apitickets < Grape::API
      version 'v1'
      format :json
      prefix :api
      resource :ticket do
        desc ' Return all the tickes in the database'
        get '', root: :ticket do
          Ticket.all
        end

        desc 'Get the ticket of the user'
        params do
          requires :cname, type: String
        end
        get ':fetchticket' do
          user1 = User.find_by(name: params[:cname])
          veh2 = Vehicle.find_by(user_id: user1.id)
          park1 = Park.find_by(vehicle_id: veh2.id)
          debugger
          Ticket.find_by(vehicle_id: veh2.id, park_id: park2.id)
        end
      end
    end
  end
end
