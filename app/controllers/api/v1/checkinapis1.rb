# frozen_string_literal: true

require_relative '../application/presenters/checkin_api'
module API
  module V1
    # this is class belongs
    class Checkinapis1 < Grape::API
      version 'v1'
      prefix 'api'
      format :json
      resource :parks do
        desc 'check out the user from the ticket table counter'
        params do
          requires :plateno, type: Integer
        end
        get 'checkin' do
          API::Application::Presenters::CheckinAPI.new(params[:plateno]).register
        end
      end
    end
  end
end
