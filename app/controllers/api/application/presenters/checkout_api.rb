# frozen_string_literal: true

module API
  module Application
    module Presenters
      class TicketIdNotFound < StandardError
        def message
          'Ticket Id does not exsist'
        end
      end

      class ParkingNotFound < StandardError
        def message
          'The car is not parked in the parking lot'
        end
      end

      class CheckoutAPI
        attr_reader :tid

        def initialize(tid)
          @tid = tid
          ticket = Ticket.find_by(id: tid)
          raise TicketIdNotFound if ticket.nil?

          @ticket = ticket
          park = Park.find_by(vehicle_id: ticket.vehicle_id)
          raise ParkingNotFound if park.nil?

          @park = park
        end

        def remove
          @park.vehicle_id = 0
          @park.save!
          @ticket.checkout = DateTime.now
          @ticket.save!
        end
      end
    end
  end
end
