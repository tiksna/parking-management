# frozen_string_literal: true

module API
  module Application
    module Presenters
      class VehicleNotFound < StandardError
        def message
          'Vehicle Is Not Available'
        end
      end

      class ParkLocationNotAvailable < StandardError
        def message
          'Parking location is preoccupied'
        end
      end


      class CarAlreadyParked < StandardError
        def message
          'Car is already parked in the parking lot '
        end
      end

      class CheckinAPI
        attr_reader :pnumber

        def initialize(pnumber)
          @pnumber = pnumber
          vehicle = Vehicle.find_by(plateno: pnumber)
          if vehicle.nil?
            raise VehicleNotFound
          else
            @vehicle = vehicle
            park = Park.find_by(vehicle_id: 0)
            if park.nil? # write the error here
              raise ParkLocationNotAvailable
            else
              @park = park
              exsist = Park.where(vehicle_id: vehicle.id)
              raise CarAlreadyParked unless exsist.nil?
            end
          end
        end

        def register
          @park.vehicle_id = @vehicle.id
          @park.save!
          Ticket.create(checkin: DateTime.now, checkout: DateTime.now, pay: 80, vehicle_id: @vehicle.id,
                        park_id: @park.id)
        end
      end
    end
  end
end
