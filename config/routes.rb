# frozen_string_literal: true

Rails.application.routes.draw do
  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'
  mount API::Base, at: '/'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
