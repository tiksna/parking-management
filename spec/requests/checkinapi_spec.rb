# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Checkinapis', type: :request do
  describe 'GET /index' do
    it 'check in the vehicle' do
      get 'http://localhost:3000/api/v1/parking/checkin', params: {
        plateno: 840
      }
      expect(response).to be_successful
    end
    it 'if plateno not passed' do
      get 'http://localhost:3000/api/v1/parks/checkin'
      expect(response).to have_http_status(400)
    end
    it 'if the wrong format is passed' do
      get 'http://localhost:3000/api/v1/parks/checkin', params: {
        plateno: 'numberoftheplate'
      }
      expect(response).to raise_error
    end
    it 'if the special character is passed' do
      get 'http://localhost:3000/api/v1/parks/checkin', params: {
        plateno: '%&^'
      }
      expect(response).to raise_error
    end
    it 'the plate number does no exsist' do
      get 'http://localhost:3000/api/v1/parks/checkin', params: {
        plateno: 13_123
      }
      expect(response).to raise_error
    end
  end
end
