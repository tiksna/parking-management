# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Apiusers', type: :request do
  describe 'GET /index' do
    it 'return the successful response' do
      get 'http://localhost:3000/api/v1/users'
      expect(response).to be_successful
    end
    it 'check the usercreate api ' do
      get 'http://localhost:300/api/v1/users', params: {
        cname: 'Shubham',
        cemail: 'Shubham@gmail.com',
        cphone: 223_344
      }
      expect(response).to be_successful
    end
  end
end
