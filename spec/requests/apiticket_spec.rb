# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Apitickets', type: :request do
  describe 'GET /index' do
    it 'check the fetchticket end point with params' do
      get 'http://localhost:3000/api/v1/ticket/fetchticket', params: {
        cname: 'Amit'
      }
      expect(response).to be_successful
    end
    it 'check if no params are passed' do
      get 'http://localhost:3000/api/v1/ticket/fetchticket'
      expect(response).to have_http_status(400)
    end
  end
end
