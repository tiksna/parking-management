# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Checkoutapis', type: :request do
  describe 'GET /index' do
    it 'checkout successfully by using valid parameters' do
      get 'http://localhost:3000/api/v1/parks/checkout', params: {
        ticketID: 1
      }
      expect(response).to be_successful
    end
    it 'checkout is no parameters are passed' do
      get 'http://localhost:3000/api/v1/parks/checkout'
      expect(response).to have_http_status(400)
    end
    it 'wrong parameter passed' do
      get 'http://localhost:3000/api/v1/parks/checkout', params: {
        ticketID: 1.1
      }
      expect(response).to raise_error
    end
    it 'if the id does not exsist' do
      get 'http://localhost:3000/api/v1/parks/checkout', params: {
        ticketID: 10_000
      }
      expect(response).to raise_error
    end
    it 'if the id clashes again ' do
      get 'http://localhost:3000/api/v1/parks/checkout', params: {
        ticketID: 'Idoftheticket'
      }
      expect(response).to raise_error
    end
    it 'if an special symbol is passed' do
      get 'localhost:3000/api/v1/parks/ceckout', params: {
        ticketID: '@$@'
      }
      expect(response).to raise_error
    end
  end
end
