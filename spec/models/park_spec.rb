# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Park, type: :model do
  context 'validation test' do
    it 'floor presence check' do
      park = Park.new(block: 12).save
      expect(park).to eq(false)
    end

    it 'block check presence' do
      park = Park.new(floor: 1).save
      expect(park).to eq(false)
    end

    it 'save the entry in the table' do
      user1 = User.create(name: 'Amit', email: 'Amit@gmail.com', phone: 11_222)
      v = Vehicle.create(plateno: 123, vehicleType: 'Bike', user: user1)
      park = Park.create(floor: 1, block: 50, vehicle: v).save
      expect(park).to eq(true)
    end
  end
end
