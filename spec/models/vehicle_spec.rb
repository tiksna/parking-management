# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Vehicle, type: :model do
  context 'validation testing' do
    it 'check the plateno' do
      veh = Vehicle.new(vehicleType: 'Bike').save
      expect(veh).to eq(false)
    end

    it ' check the vehicleType' do
      veh = Vehicle.new(plateno: 123).save
      expect(veh).to eq(false)
    end

    it 'save the entry success' do
      user1 = User.create(name: 'AmitKumar', email: 'Amit@gmail.com', phone: 1234)
      veh = Vehicle.new(plateno: 123, vehicleType: 'Bike', user: user1).save
      expect(veh).to eq(true)
    end
  end
end
