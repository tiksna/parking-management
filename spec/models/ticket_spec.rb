# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Ticket, type: :model do
  context 'validation test' do
    it 'checkin time ' do
      ticket = Ticket.new(pay: 80).save
      expect(ticket).to eq(false)
    end
  end
end
