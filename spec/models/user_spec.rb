# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  context 'validation test' do
    it 'ensures the presence of name' do
      user = User.new(email: 'Amit@gmail.com', phone: 2234).save
      expect(user).to eq(false)
    end
    it ' ensures the presence of email' do
      user = User.new(name: 'Amit', phone: 22_321).save
      expect(user).to eq(false)
    end

    it ' ensures the presence of phone' do
      user = User.new(name: 'Amit', email: 'Amit@gmail.com').save
      expect(user).to eq(false)
    end

    it 'Save the entry' do
      user = User.new(name: 'Amit', email: 'Amit@gmail.com', phone: 22_321).save
      expect(user).to eq(true)
    end
  end
end
