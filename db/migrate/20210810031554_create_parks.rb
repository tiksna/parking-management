# frozen_string_literal: true

class CreateParks < ActiveRecord::Migration[6.1]
  def change
    create_table :parks do |t|
      t.integer :floor
      t.integer :block
      t.belongs_to :vehicle, index: true
      t.timestamps
    end
  end
end
