# frozen_string_literal: true

class ChangeColumn < ActiveRecord::Migration[6.1]
  def change
    change_column :parks, :block, :Integer, default: false
  end
end
