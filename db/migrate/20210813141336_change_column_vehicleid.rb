# frozen_string_literal: true

class ChangeColumnVehicleid < ActiveRecord::Migration[6.1]
  def change
    change_column :parks, :vehicle_id, :bigint, default: false
  end
end
