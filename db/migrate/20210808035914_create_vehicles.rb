# frozen_string_literal: true

class CreateVehicles < ActiveRecord::Migration[6.1]
  def change
    create_table :vehicles do |t|
      t.integer :plateno
      t.string :vehicleType
      t.belongs_to :user, index: true
      t.timestamps
    end
  end
end
