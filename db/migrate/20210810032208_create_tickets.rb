# frozen_string_literal: true

class CreateTickets < ActiveRecord::Migration[6.1]
  def change
    create_table :tickets do |t|
      t.datetime :checkin
      t.datetime :checkout
      t.float :pay
      t.belongs_to :vehicle, index: true
      t.belongs_to :park, index: true

      t.timestamps
    end
  end
end
